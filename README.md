# FlickrToLJ

## Не поддерживается

* после коммерциализации Flickr получение прямых ссылок на фото не работает корректно;
* если включена двухфакторная авторизация через Я.Ключ, для авторизации на Яндекс.Диск необходимо создать пароль в [пароли приложений](https://passport.yandex.ru/profile).

## Скачать сборку под Windows 64bit c Яндекс.Диска

[FlickrToLJ 2.6.3](https://yadi.sk/d/DUni23Xw3VX4HJ)

## Лицензия

Исходный код приложения предоставляется по лицензии [GPLv3](https://bitbucket.org/eigensinn/flickr_to_lj/src/master/LICENSE.md).

## Возможности

Приложение предоставляет:

* получать прямые ссылки (direct links) на изображения, включенные в альбомы [Flickr.com](https://www.flickr.com/);
* генерировать HTML-код (или BB-код) для вставки на сторонние сайты;
* передавать альбомы изображений с [Яндекс.Диск](https://disk.yandex.ru/) на Flickr.

Приложение написано с использованием [PyQt5](https://www.riverbankcomputing.com/software/pyqt/download5)

GUI создан с помощью Qt Designer.

## Примеры преобразования ui в py с помощью pyuic

### Windows
    
    cd C:\Python36\Lib\site-packages\PyQt5\uic
    pyuic5 ui_main.ui -o ui_main.py

### Linux

    cd /home/alex/PycharmProjects/flickr_to_lj
    pyuic5 ui_main.ui -o ui_main.py

## Сборка с помощью PyInstaller под Windows

Перед сборкой следует добавить свои api_key и api_secret в файл [requisites.py](https://bitbucket.org/eigensinn/flickr_to_lj/src/ca04367c1f65cc1b0e50d6e78e05b18f83ccbcd6/requisites.py?at=master&fileviewer=file-view-default), предварительно [зарегистрировав приложение на Flickr](https://www.flickr.com/services/apps/create/noncommercial/)

    api_key = u'55555d6a7777b999e69d76c5e42ecd55'
    api_secret = u'333de9c44400888a'
    
Процесс сборки желательно осуществлять при установленных компонентах api-ms-win-core- и api-ms-win-crt- Visual Studio 2015 (C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\Remote Debugger\x64)

    cd path\to\flickr_to_lj
    path\to\flickr_to_lj>venv\Scripts\activate
    (venv) path\to\flickr_to_lj>venv\Scripts\pyinstaller --onefile msvc.spec
    
## Образец ответа сервера Flickr при загрузке изображения

    b'<?xml version="1.0" encoding="utf-8" ?>\n<rsp stat="ok">\n<photoid>26990411398</photoid>\n</rsp>\n'

## Сторонние компоненты

Использованы иконки [Open Iconic](https://github.com/iconic/open-iconic)
