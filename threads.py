import sys
import json
import io
import os
import re
from PIL import Image
from lxml import etree
from urllib import request
from distutils.util import strtobool
from pathlib import Path
from PyQt5.QtCore import pyqtSignal, QSettings, QThread
from YaDiskClient.YaDiskClient import YaDiskException

home = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"


# Декоратор, выполняющий upload установленное количество раз до успешной загрузки файла на Flickr
def try_repeat(func):
    def wrapper(*args, **kwargs):
        count = parent.mw.ui.le_repeat.text()
        attempt = 1
        while count:
            try:
                return func(*args, **kwargs)
            except Exception as e:
                if parent.mw.ui.rb_err_log.isChecked():
                    with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                        w_file.write("Пробую повторно передать файл "+str(e)+"на Flickr. Попытка № "+str(attempt))
                count -= 1
                attempt += 1
    return wrapper


class GetPhotosThread(QThread):
    textedit_paste = pyqtSignal(bytes, str, str, str, str, str)

    def __init__(self, parent, selected_rows):
        QThread.__init__(self, parent)
        self.selected_rows = selected_rows

    def run(self):
        for i in self.selected_rows:
            # Получаем фотки внутри данного фотосета по его id
            ps_id = str(parent.mw.ui.tableWidget.item(i, 0).text())
            photos = parent.mw.flickr.photosets.getPhotos(photoset_id=ps_id, \
                                                user_id=parent.mw.user_id)
            if photos['stat'] == 'ok':
                photo_ids = []
                # Получаем id всех фото внутри данного фотосета, добавляем в список
                for photo in photos["photoset"]["photo"]:
                    photo_ids.append({"id": photo["id"], "title": photo["title"]})
                # Создаем список отмеченных чекбоксов (размеров)
                checks = []
                for box in parent.mw.checkboxes:
                    if box.isChecked():
                        checks.append(box.text())
                checks_str = str(checks)
                # Проверяем формат вывода
                if parent.mw.ui.rb_bbcode_image.isChecked():
                    format_left = '[img]'
                    format_right = '[/img]'
                elif parent.mw.ui.rb_bbcode_link.isChecked():
                    format_left = '[url]'
                    format_right = '[/url]'
                elif parent.mw.ui.rb_other.isChecked():
                    format_left = parent.mw.ui.le_format_left.text()
                    format_right = parent.mw.ui.le_format_right.text()

                # Получаем direct_link на каждое фото данного фотосета
                for p_id in photo_ids:
                    sizes_json = parent.mw.flickr_json.photos.getSizes(photo_id=p_id["id"])
                    sizes = json.loads(parent.mw.check_ver(sizes_json))
                    if sizes['stat'] == 'ok':
                        alt, title = '', ''
                        if parent.mw.ui.cb_alt.isChecked():
                            title = ' alt="' + p_id["title"] + '" '
                        if parent.mw.ui.cb_title.isChecked():
                            alt = 'title="' + p_id["title"] + '"'
                        # Проверяем формат вывода
                        if parent.mw.ui.rb_image.isChecked():
                            format_left = '<img ' + alt + title + ' src="'
                            format_right = '">'
                        elif parent.mw.ui.rb_link.isChecked():
                            format_left = '<a ' + title + ' href="'
                            format_right = '"></a>'
                        self.textedit_paste.emit(sizes_json, checks_str, format_left, format_right, ps_id, p_id["id"])


# Поток получения данных альбомов
class GetAlbumsThread(QThread):
    table_fill = pyqtSignal(bytes)

    def __init__(self, parent):
        QThread.__init__(self, parent)

    def run(self):
        # Получаем фотосеты
        sets_json = parent.mw.flickr_json.photosets.getList(user_id=parent.mw.user_id)
        sets = json.loads(parent.mw.check_ver(sets_json))
        if sets['stat'] == 'ok':
            self.table_fill.emit(sets_json)


# Поток передачи изображения с Яндекс.Диск на Flickr
class MoveToFlickr(QThread):
    completed = pyqtSignal(str, int)
    display_file = pyqtSignal(str)
    display_error = pyqtSignal(str, str)

    def __init__(self, parent, selected_rows):
        QThread.__init__(self, parent)
        self.selected_rows = selected_rows
        self.folder_name = None
        self.ps = None

    def run(self):
        for i in self.selected_rows:
            path = parent.mw.ui.tableWidget_2.item(i, 0).text()
            displayname = parent.mw.ui.tableWidget_2.item(i, 2).text()
            is_dir = parent.mw.ui.tableWidget_2.item(i, 4).text()
            # Если скачиваем файл
            if strtobool(is_dir) is False:
                ext = displayname.split(".")[-1]
                if ext in ['jpeg', 'jpg', 'png', 'gif']:
                    try:
                        os_file = os.path.join(home + displayname)
                        os_path = Path(os_file)
                        # Чтобы повторно не загружать файл после сбоя
                        if not os_path.is_file():
                            parent.mw.disk.download(path, os_file)
                    except (Exception, YaDiskException) as exc:
                        if parent.mw.ui.rb_err_messagebox.isChecked():
                            self.display_error.emit(exc.__class__.__name__ + ' | ' + displayname, str(exc))
                        elif parent.mw.ui.rb_err_log.isChecked():
                            with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                                w_file.write(exc.__class__.__name__ + ' | ' + displayname + ' | ' + str(exc) + '\n')
                    else:
                        self.display_file.emit(displayname)
                        self.completed.emit("download", 100)
                        self.upload(displayname, 0)
                        # Удаляем файл, загруженный с Яндекс.диск на накопитель
                        os.remove(os_file)
            # Если скачиваем файлы из папки
            else:
                try:
                    resp = parent.mw.disk.ls(path)
                except (Exception, YaDiskException) as exc:
                    if parent.mw.ui.rb_err_messagebox.isChecked():
                        self.display_error.emit(exc.__class__.__name__ + ' | ' + displayname, str(exc))
                    elif parent.mw.ui.rb_err_log.isChecked():
                        with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                            w_file.write(exc.__class__.__name__ + ' | ' + displayname + ' | ' + str(exc) + '\n')
                else:
                    l = len(resp)
                    for ind, file in enumerate(resp):
                        if file['isDir'] is True:
                            self.folder_name = file['displayname']
                    for j, file in enumerate(resp):
                        if file['isDir'] is False:
                            ext = file['displayname'].split(".")[-1]
                            if ext in ['jpeg', 'jpg', 'png', 'gif']:
                                try:
                                    os_file = os.path.join(home + file['displayname'])
                                    os_path = Path(os_file)
                                    # Чтобы повторно не загружать файл после сбоя
                                    if not os_path.is_file():
                                        parent.mw.disk.download(file['path'], os_file)
                                except (Exception, YaDiskException) as exc:
                                    if parent.mw.ui.rb_err_messagebox.isChecked():
                                        self.display_error.emit(exc.__class__.__name__ + ' | ' + file['displayname'],
                                                                str(exc))
                                    elif parent.mw.ui.rb_err_log.isChecked():
                                        with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                                            w_file.write(exc.__class__.__name__ + ' | ' + file['displayname'] + ' | ' + str(exc) + '\n')
                                else:
                                    value = int((j + 1) * 100 / l)
                                    self.display_file.emit(file['displayname'])
                                    self.completed.emit("download", value)
                                    self.upload(file['displayname'], j)
                                    # Удаляем файлы, загруженные с Яндекс.диск на накопитель
                                    os.remove(os_file)
                self.folder_name = None

    @try_repeat
    def upload(self, displayname, photo_number):
        # Определяем объект со свойствами, который будет передаваться на flickr
        fileobj = FileWithCallback(displayname, self.callback)
        # Загружаем файл и получаем ответ для анализа
        rsp = parent.mw.flickr.upload(displayname, fileobj, format="rest")
        # С помощью lxml Получаем photo_id нового изображения из ответа сервера
        tree = etree.fromstring(rsp)
        # Получаем статус ответа
        if tree.xpath('//rsp')[0].get('stat') == 'ok':
            new_id = tree.xpath('//rsp/photoid/text()')[0]
            # Если фото из папки
            if not self.folder_name is None:
                # Если загружаем первое по счету фото
                if photo_number == 1:
                    # Создаем папку на Flickr
                    self.ps = parent.mw.flickr.photosets.create(title=self.folder_name, primary_photo_id=new_id)
                    if self.ps['stat'] == 'fail':
                        title = "Не удалось создать альбом {0}".format(self.folder_name)
                        err = "код ошибки: {1} | сообщение: {2}".format(self.ps['code'], self.ps['message'])
                        if parent.mw.ui.rb_err_messagebox.isChecked():
                            self.display_error.emit(title, err)
                        elif parent.mw.ui.rb_err_log.isChecked():
                            with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                                w_file.write(title + ' | ' + err + '\n')
                else:
                    # Добавляем измененные фото в альбом
                    resp = parent.mw.flickr.photosets.addPhoto(photoset_id=self.ps['photoset']['id'], photo_id=new_id)
                    if resp['stat'] == 'fail':
                        title = "Не удалось добавить фото {0} в альбом".format(displayname)
                        err = "код ошибки: {1} | сообщение: {2}".format(resp['code'], resp['message'])
                        if parent.mw.ui.rb_err_messagebox.isChecked():
                            self.display_error.emit(title, err)
                        elif parent.mw.ui.rb_err_log.isChecked():
                            with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                                w_file.write(title + ' | ' + err + '\n')
        else:
            title = "Не удалось загрузить файл {0} на Flickr".format(displayname)
            err = "код ошибки: {1} | сообщение: {2}".format(tree.xpath('//rsp')[0].get('code'),
                                                            tree.xpath('//rsp')[0].get('message'))
            if parent.mw.ui.rb_err_messagebox.isChecked():
                self.display_error.emit(title, err)
            elif parent.mw.ui.rb_err_log.isChecked():
                with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                    w_file.write(title + ' | ' + err + '\n')
            raise Exception(displayname)
        fileobj.file.close()

    def callback(self, value):
        self.completed.emit("upload", value)


class FileWithCallback(object):
    def __init__(self, filename, callback):
        self.file = open(filename, 'rb')
        self.callback = callback
        self.len = os.path.getsize(filename)
        self.fileno = self.file.fileno
        self.tell = self.file.tell

    def read(self, size):
        if self.callback:
            self.callback(self.tell() * 100 // self.len)
        return self.file.read(size)


# Поток изменения размера изображения
class ResizePhotoThread(QThread):
    textedit_fill = pyqtSignal(bytes, str, str, str)
    le_filename_fill = pyqtSignal(str)
    display_error = pyqtSignal(str, str)
    pbar_upload_value = pyqtSignal(int)

    def __init__(self, parent, original_url, name_with_ext, ps_id, p_id, checks_str, \
                 format_left, format_right):
        QThread.__init__(self, parent)
        self.original_url = original_url
        self.name_with_ext = name_with_ext
        self.ps_id = ps_id
        self.p_id = p_id
        self.checks_str = checks_str
        self.format_left = format_left
        self.format_right = format_right

    def run(self):
        # Получаем файл с flickr по direct link
        with request.urlopen(self.original_url) as url:
            f = io.BytesIO(url.read())
        # Скрываем фото из общего доступа
        # mw.flickr.photos.setPerms(photo_id=p_id, is_public=0, is_friend=0, is_family=0)
        # Открываем файл из оперативной памяти в PIL
        img = Image.open(f)
        # Определяем размеры изображения
        original_width, original_height = img.size
        # Определяем новые размеры изображения
        new_width = int(parent.mw.ui.combo_resize.currentText())
        new_height = int(new_width * original_height / original_width)
        # Изменяем размеры изображения
        resized_image = img.resize((new_width, new_height), Image.LANCZOS)
        # Определяем расширение имени файла
        img_type = self.name_with_ext.split(".")[-1]
        if img_type == "jpg":
            img_type = "JPEG"
        # Сохраняем измененное изображение
        resized_image.save(self.name_with_ext, img_type)
        new_id = self.upload_photo()
        self.repeat_get_size(new_id)

    @try_repeat
    def upload_photo(self):
        self.le_filename_fill.emit(self.name_with_ext)
        # Определяем объект со свойствами, который будет передаваться на flickr
        fileobj = FileWithCallback(self.name_with_ext, self.callback)
        # Загружаем файл и получаем ответ для анализа
        rsp = parent.mw.flickr.upload(self.name_with_ext, fileobj, format="rest")
        # С помощью lxml Получаем photo_id нового изображения из ответа сервера
        tree = etree.fromstring(rsp)
        # Получаем статус ответа
        if tree.xpath('//rsp')[0].get('stat') == 'ok':
            new_id = tree.xpath('//rsp/photoid/text()')[0]
            # Добавляем измененные фото в альбом
            parent.mw.flickr.photosets.addPhoto(photoset_id=self.ps_id, photo_id=new_id)
            if parent.mw.ui.cb_move_original.isChecked():
                # Перемещаем оригинальное изображение из альбома
                parent.mw.flickr.photosets.removePhoto(photoset_id=self.ps_id, photo_id=self.p_id)
            # Закрываем файл
            fileobj.file.close()
            # Удаляем файлы, загруженные с flickr на накопитель
            os.remove(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])) + "/" + self.name_with_ext))
            self.le_filename_fill.emit("clear")
            self.pbar_upload_value.emit(100)
            return new_id
        else:
            title = "Не удалось загрузить фото {0} на Flickr".format(self.name_with_ext)
            err = "код ошибки: {1} | сообщение: {2}".format(tree.xpath('//rsp')[0].get('code'),
                                                            tree.xpath('//rsp')[0].get('message'))
            if parent.mw.ui.rb_err_messagebox.isChecked():
                self.display_error.emit(title, err)
            elif parent.mw.ui.rb_err_log.isChecked():
                with open(os.path.join(home + 'log.txt'), 'a') as w_file:
                    w_file.write(title + ' | ' + err + '\n')
            raise Exception(self.name_with_ext)

    def callback(self, progress):
        self.pbar_upload_value.emit(progress)

    def repeat_get_size(self, new_id):
        # Получаем новые url измененных изображений
        new_sizes_json = parent.mw.flickr_json.photos.getSizes(photo_id=new_id)
        self.textedit_fill.emit(new_sizes_json, self.checks_str, self.format_left, self.format_right)


# Поток получения id пользователя
class SaveIdThread(QThread):
    def __init__(self, parent, address_to_verify):
        QThread.__init__(self, parent)
        self.address_to_verify = address_to_verify

    def run(self):
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.address_to_verify)
        if match == None:
            people = parent.mw.flickr.people.findByUsername(username=self.address_to_verify)
        else:
            people = parent.mw.flickr.people.findByEmail(find_email=self.address_to_verify)
        if people['stat'] == 'ok':
            parent.mw.user_id = people['user']['id']
            settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                                 QSettings.IniFormat)
            arr = [("user_id", parent.mw.user_id), ("username_or_email", self.address_to_verify)]
            for i, j in arr:
                settings.setValue(i, j)
