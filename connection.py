from urllib import request
from urllib.error import HTTPError, URLError


# Проверка подключения к интернету
def internet_on():
    try:
        request.urlopen('http://yandex.ru', timeout=1)
        return True
    except (HTTPError, URLError):
        return False