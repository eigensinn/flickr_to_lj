import sys
import os
import json
import flickrapi
import pickle
from urllib.parse import unquote
from PyQt5.QtWidgets import QMainWindow, QApplication, QTableWidgetItem, QMessageBox, \
    QHeaderView
from PyQt5.QtGui import QTextCursor
from PyQt5.QtCore import QSettings
from ui_main import Ui_MainWindow
from distutils.util import strtobool
from YaDiskClient.YaDiskClient import YaDisk, YaDiskException
from cryptography.fernet import Fernet
from connection import internet_on
try:
    import requisites_dev as req
except ImportError:
    import requisites as req
import threads
threads.parent = sys.modules[__name__]


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.tableWidget.setColumnCount(7)
        self.ui.tableWidget.setHorizontalHeaderLabels(['id', 'server', \
                                                       'farm', 'photos', 'videos', 'title', 'description'])
        self.ui.tableWidget_2.setColumnCount(5)
        self.ui.tableWidget_2.setHorizontalHeaderLabels(['path', 'creationdate', \
                                                       'displayname', 'lastmodified', 'isDir'])
        self.api_key = req.api_key
        self.api_secret = req.api_secret
        self.checkboxes = [self.ui.cb_square, self.ui.cb_large_square, self.ui.cb_thumbnail, self.ui.cb_small, \
                           self.ui.cb_small_320, self.ui.cb_medium, self.ui.cb_medium_640, self.ui.cb_medium_800, \
                           self.ui.cb_large, self.ui.cb_large_1600, self.ui.cb_large_2048, self.ui.cb_original, \
                           self.ui.cb_move_original, self.ui.cb_own_size]
        self.attributes = [self.ui.cb_alt, self.ui.cb_title]
        self.radios = [self.ui.rb_image, self.ui.rb_link, self.ui.rb_other, self.ui.rb_bbcode_image, \
                       self.ui.rb_bbcode_link, self.ui.rb_err_messagebox, self.ui.rb_err_log, self.ui.rb_err_none]
        self.formats = [self.ui.le_format_left, self.ui.le_format_right]
        self.flickr = flickrapi.FlickrAPI(self.api_key, self.api_secret, format='parsed-json')
        self.flickr_json = flickrapi.FlickrAPI(self.api_key, self.api_secret, format='json')
        self.ya_pass, self.ya_email, self.disk, self.home = None, None, None, None
        self.breadcrumbs = []
        self.offset = 0
        self.current_folder = '/'
        self.ui.tableWidget_2.setColumnWidth(0, 200)
        header = self.ui.tableWidget_2.horizontalHeader()
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        try:
            settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), \
                                              "settings.ini"), QSettings.IniFormat)
            self.user_id = settings.value("user_id")
            self.ui.le_username.setText(settings.value("username_or_email"))
            self.ui.le_repeat.setText(settings.value("le_repeat"))
            self.restoreState(settings.value("windowstate"))
            self.restoreGeometry(settings.value("geo"))
            for box in self.checkboxes + self.attributes:
                try:
                    box.setChecked(strtobool(settings.value(box.objectName())))
                except AttributeError:
                    pass
            for radio in self.radios:
                try:
                    radio.setChecked(strtobool(settings.value(radio.objectName())))
                except AttributeError:
                    pass
            combo_resize = self.ui.combo_resize
            combo_resize.setCurrentIndex(int(settings.value(combo_resize.objectName())))
            for f in self.formats:
                f.setText(settings.value(f.objectName()))
            self.home = settings.value("home")
        # Если программа запускается в первый раз и настройки еще не сохранялись
        except TypeError:
            pass
        else:
            self.ya_restore()
        self.original_check()
        self.ui.pb_albums.clicked.connect(self.get_albums)
        self.ui.pb_code.clicked.connect(self.get_photos)
        self.ui.pb_user_id.clicked.connect(self.save_id)
        self.ui.tb_copy.clicked.connect(self.copy_code)
        self.ui.tb_clear.clicked.connect(self.clear_textedit)
        self.ui.cb_original.stateChanged.connect(self.original_check)
        self.ui.pb_save_ya_auth.clicked.connect(self.ya_auth)
        self.ui.pb_move_to_flickr.clicked.connect(self.move_to_flickr)
        self.ui.pb_flickr_auth.clicked.connect(self.flickr_auth)
        self.ui.act_about.triggered.connect(self.about)
        for f in self.formats:
            f.editingFinished.connect(self.save_settings)
        for box in self.checkboxes + self.attributes:
            box.stateChanged.connect(self.save_settings)
        for r in self.radios:
            r.toggled.connect(self.save_settings)
        self.ui.combo_resize.currentIndexChanged.connect(self.save_settings)
        self.ui.le_repeat.textChanged.connect(self.save_settings)
        self.ui.tableWidget_2.cellDoubleClicked.connect(self.go_deeper)
        self.ui.tb_root.clicked.connect(self.get_ya_folders)
        self.ui.tb_home.clicked.connect(self.get_home)
        self.ui.tb_set_home.clicked.connect(self.set_home)
        self.ui.tb_to_folder.clicked.connect(self.go_deeper_by_item)
        self.ui.tb_up.clicked.connect(self.up)
        self.ui.tb_right.clicked.connect(self.to_right)
        self.ui.tb_left.clicked.connect(self.to_left)

    # Проверяем состояние чекбокса self.ui.cb_original
    def original_check(self):
        if self.ui.cb_original.isChecked():
            self.ui.gb_resize.setEnabled(True)
        else:
            self.ui.gb_resize.setEnabled(False)

    # Проверяем версию python (< 3.6 требуется преобразование bytes в str для json)
    def check_ver(self, string):
        if sys.version_info < (3, 6):
            return string.decode('utf-8')
        else:
            return string

    # Запускаем поток получения альбомов
    def get_albums(self):
        self.get_albums_th = threads.GetAlbumsThread(self)
        self.get_albums_th.table_fill.connect(self.table_fill)
        self.get_albums_th.start()

    # Заполняем таблицу полученными данными альбомов
    def table_fill(self, sets_json):
        sets = json.loads(self.check_ver(sets_json))
        self.ui.tableWidget.setRowCount(0)
        for i, v in enumerate(sets['photosets']['photoset']):
            self.ui.tableWidget.insertRow(self.ui.tableWidget.rowCount())
            self.ui.tableWidget.setItem(i, 0, QTableWidgetItem(v['id']))
            self.ui.tableWidget.setItem(i, 1, QTableWidgetItem(v['server']))
            self.ui.tableWidget.setItem(i, 2, QTableWidgetItem(str(v['farm'])))
            self.ui.tableWidget.setItem(i, 3, QTableWidgetItem(str(v['photos'])))
            self.ui.tableWidget.setItem(i, 4, QTableWidgetItem(str(v['videos'])))
            self.ui.tableWidget.setItem(i, 5, QTableWidgetItem(str(v['title']['_content'])))
            self.ui.tableWidget.setItem(i, 6, QTableWidgetItem(str(v['description']['_content'])))

    # Запускаем поток получения фото, передаем в него id альбомов выделенных строк таблицы
    def get_photos(self):
        selected_rows = []
        for i in self.ui.tableWidget.selectedIndexes():
            selected_rows.append(i.row())
        if not selected_rows == []:
            self.get_photos_th = threads.GetPhotosThread(self, selected_rows)
            self.get_photos_th.textedit_paste.connect(self.textedit_paste)
            self.get_photos_th.start()

    # Отображаем текущее имя обрабатываемого изображения
    def le_filename_fill(self, name_with_ext):
        if name_with_ext == "clear":
            self.ui.le_filename.clear()
        else:
            self.ui.le_filename.setText(name_with_ext)

    # Устанавливаем значение прогресс-бара при обработке изображения
    def pbar_upload_value(self, value):
        self.ui.pbar_upload.setValue(value)

    # Вставляем код, сформированный из ссылок, в консоль
    def textedit_paste(self, sizes_json, checks_str, format_left, format_right, ps_id, p_id):
        sizes = json.loads(self.check_ver(sizes_json))
        for s in sizes["sizes"]["size"]:
            # Если имя размера совпадает с именем отмеченного чекбокса
            if s["label"] in checks_str:
                # Проверяем, преобразовывать изображения с помощью PIL или нет
                if self.ui.cb_own_size.isChecked() and self.ui.cb_original.isChecked():
                    name_with_ext = s["source"].split("/")[-1]
                    name = name_with_ext.split(".")
                    # Если url принадлежит оригинальному изображению
                    if name[0].endswith('_o'):
                        # Преобразуем изображение в потоке ResizePhotoThread
                        self.resize_photo_th = threads.ResizePhotoThread(self, s["source"], name_with_ext, ps_id, p_id, \
                                                                 checks_str, format_left, format_right)
                        self.resize_photo_th.textedit_fill.connect(self.textedit_fill)
                        self.resize_photo_th.display_error.connect(self.display_error)
                        self.resize_photo_th.le_filename_fill.connect(self.le_filename_fill)
                        self.resize_photo_th.pbar_upload_value.connect(self.pbar_upload_value)
                        self.resize_photo_th.start()
                else:
                    self.ui.textEdit.insertPlainText(format_left + s["source"] + format_right)
                    self.ui.textEdit.moveCursor(QTextCursor.End)

    # Отображаем ошибку
    def display_error(self, type, err):
        QMessageBox.warning(self, type, err)
        #self.ui.textEdit.append(err)

    # Заполняем textEdit полученным кодом
    def textedit_fill(self, new_sizes_json, checks_str, format_left, format_right):
        new_sizes = json.loads(mw.check_ver(new_sizes_json))
        for ns in new_sizes["sizes"]["size"]:
            if ns["label"] in checks_str:
                self.ui.textEdit.insertPlainText(format_left + ns["source"] + format_right)
                self.ui.textEdit.moveCursor(QTextCursor.End)

    # Событие закрытия окна. Сохраняем размеры и состояние виджетов
    def closeEvent(self, event):
        settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                             QSettings.IniFormat)
        arr = [("windowstate", self.saveState()), ("geo", self.saveGeometry())]
        for i, j in arr:
            settings.setValue(i, j)
        QMainWindow.closeEvent(self, event)

    # Запускаем поток получения id пользователя
    def save_id(self):
        address_to_verify = self.ui.le_username.text()
        self.save_id_th = threads.SaveIdThread(self, address_to_verify)
        self.save_id_th.start()

    # Сохраняем состояния радио, чекбоксов, комбо-боксов
    def save_settings(self):
        settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                             QSettings.IniFormat)
        arr = []
        for box in self.checkboxes +self.attributes:
            arr.append((box.objectName(), box.isChecked()))
        for radio in self.radios:
            arr.append((radio.objectName(), radio.isChecked()))
        for f in self.formats:
            arr.append((f.objectName(), f.text()))
        arr.append(("combo_resize", self.ui.combo_resize.currentIndex()))
        arr.append(("le_repeat", self.ui.le_repeat.text()))
        for i, j in arr:
            settings.setValue(i, j)

    # Копируем код из консоли в буфер обмена
    def copy_code(self):
        self.ui.textEdit.selectAll()
        self.ui.textEdit.copy()

    # Очищаем textEdit
    def clear_textedit(self):
        self.ui.textEdit.clear()

    # Авторизация на Flickr с помощью браузера
    def flickr_auth(self):
        self.flickr.authenticate_via_browser(perms='write')

# ----------------------Яндекс.Диск-------------------------------

    # Передаем выбранные файлы с Яндекс.Диск на Flickr
    def move_to_flickr(self):
        selected_rows = []
        for i in self.ui.tableWidget_2.selectedIndexes():
            selected_rows.append(i.row())
        if not selected_rows == []:
            self.move_to_flickr_th = threads.MoveToFlickr(self, selected_rows)
            self.move_to_flickr_th.completed.connect(self.completed)
            self.move_to_flickr_th.display_error.connect(self.display_error)
            self.move_to_flickr_th.display_file.connect(self.display_file)
            self.move_to_flickr_th.start()

    # Отображаем прогресс передачи с Яндекс.Диск на Flickr
    def completed(self, pbar, value):
        if pbar == "download":
            self.ui.pbar_download.setValue(value)
        else:
            self.ui.pbar_upload_2.setValue(value)

    # Отображаем текущее имя обрабатываемого изображения при передаче с Яндекс.Диск на Flickr
    def display_file(self, file):
        self.ui.le_file_action.setText(file)

    # Сброс количества неотображаемых элементов
    def set_offset_to_zero(self):
        self.offset = 0
        self.ui.tb_left.setEnabled(False)
        self.ui.le_offset.setText('0 – 40')

    # Отбражаем следующие 40 элементов
    def to_right(self):
        resp = self.disk.ls(self.current_folder, offset=self.offset + 40, amount=40)
        self.fill_ya_table(resp)
        self.offset += 40
        self.ui.tb_left.setEnabled(True)
        self.ui.le_offset.setText(str(self.offset) + ' – ' + str(self.offset + 40))

    # Отбражаем предыдущие 40 элементов
    def to_left(self):
        resp = self.disk.ls(self.current_folder, offset=self.offset - 40, amount=40)
        self.fill_ya_table(resp)
        self.offset -= 40
        self.ui.le_offset.setText(str(self.offset) + ' – ' + str(self.offset + 40))
        if self.offset == 0:
            self.set_offset_to_zero()

    # Заполняем таблицу данными с Яндекс.Диска
    def fill_ya_table(self, resp):
        self.ui.tableWidget_2.setRowCount(0)
        for i, v in enumerate(resp):
            self.ui.tableWidget_2.insertRow(self.ui.tableWidget_2.rowCount())
            self.ui.tableWidget_2.setItem(i, 0, QTableWidgetItem(str(unquote(v['path']))))
            self.ui.tableWidget_2.setItem(i, 1, QTableWidgetItem(v['creationdate']))
            self.ui.tableWidget_2.setItem(i, 2, QTableWidgetItem(v['displayname']))
            self.ui.tableWidget_2.setItem(i, 3, QTableWidgetItem(v['lastmodified']))
            self.ui.tableWidget_2.setItem(i, 4, QTableWidgetItem(str(v['isDir'])))
        self.current_folder = self.ui.tableWidget_2.item(0, 0).text()

    # переходим в папку выше
    def up(self):
        self.disk = YaDisk(self.ya_email, self.ya_pass)
        if not self.breadcrumbs == []:
            resp = self.disk.ls(self.breadcrumbs[-1], offset=0, amount=40)
            # Удаляем нижний уровень в self.breadcrumbs
            if not self.breadcrumbs == []:
                del self.breadcrumbs[-1]
            self.fill_ya_table(resp)
        self.set_offset_to_zero()

    # Получаем папки и файлы корневого каталога Яндекс.Диска self.ui.le_root
    def get_ya_folders(self):
        if self.ya_email is not None and self.ya_pass is not None:
            self.ya_restore()
        self.disk = YaDisk(self.ya_email, self.ya_pass)
        resp = self.disk.ls("/", offset=0, amount=40)
        # Запоминаем имя предыдущей папки
        if not self.breadcrumbs == []:
            self.breadcrumbs.append(self.ui.tableWidget_2.item(0, 0).text())
        self.fill_ya_table(resp)
        self.set_offset_to_zero()

    # Получаем папки и файлы домашней директории
    def get_home(self):
        if self.ya_email is not None and self.ya_pass is not None:
            self.ya_restore()
        self.disk = YaDisk(self.ya_email, self.ya_pass)
        if self.home is not None:
            resp = self.disk.ls(self.home, offset=0, amount=40)
        else:
            resp = self.disk.ls("/", offset=0, amount=40)
        # Запоминаем имя предыдущей папки
        if not self.breadcrumbs == []:
            self.breadcrumbs.append(self.ui.tableWidget_2.item(0, 0).text())
        self.fill_ya_table(resp)
        self.set_offset_to_zero()

    # Устанавливаем домашнюю папку
    def set_home(self):
        selected_rows = []
        for i in self.ui.tableWidget_2.selectedIndexes():
            selected_rows.append(i.row())
        if selected_rows == []:
            self.home = self.ui.tableWidget_2.item(0, 0).text()
        else:
            self.home = self.ui.tableWidget_2.item(selected_rows[0], 0).text()
        settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                             QSettings.IniFormat)
        settings.setValue("home", self.home)

    # Переходим во вложенную папку на Яндекс.Диске
    def go_deeper(self, row, col):
        self.disk = YaDisk(self.ya_email, self.ya_pass)
        resp = self.disk.ls(self.ui.tableWidget_2.item(row, 0).text(), offset=0, amount=40)
        # Запоминаем имя предыдущей папки
        self.breadcrumbs.append(self.ui.tableWidget_2.item(0, 0).text())
        self.fill_ya_table(resp)
        self.set_offset_to_zero()

    # Переходим в выделенную папку по кнопке self.ui.tb_to_folder
    def go_deeper_by_item(self):
        selected_rows = []
        for i in self.ui.tableWidget_2.selectedIndexes():
            selected_rows.append(i.row())
        if not selected_rows == []:
            self.disk = YaDisk(self.ya_email, self.ya_pass)
            resp = self.disk.ls(self.ui.tableWidget_2.item(selected_rows[0], 0).text(), offset=0, amount=40)
            # Запоминаем имя предыдущей папки
            self.breadcrumbs.append(self.ui.tableWidget_2.item(0, 0).text())
            self.fill_ya_table(resp)
        self.set_offset_to_zero()

    # Авторизация на Яндекс.Диске
    def ya_login(self):
        self.disk = YaDisk(self.ya_email, self.ya_pass)
        if internet_on():
            try:
                self.disk.df()
            except YaDiskException:
                self.ui.le_ya_auth_state.setStyleSheet("color: rgb(255,0,0);")
                self.ui.le_ya_auth_state.setText("Неправильный пароль")
                return False
            else:
                self.ui.le_ya_auth_state.setStyleSheet("color: rgb(50,205,50);")
                self.ui.le_ya_auth_state.setText("Авторизация пройдена")
                self.ui.pb_move_to_flickr.setEnabled(True)
                self.ui.frame.setEnabled(True)
                return True
        else:
            self.ui.le_ya_auth_state.setStyleSheet("color: rgb(255,0,0);")
            self.ui.le_ya_auth_state.setText("Отсутствует подключение к интернету")
            return False

    # Шифрование реквизитов входа и их сохранение
    def ya_auth(self):
        self.ya_email = self.ui.le_ya_email.text()
        self.ya_pass = self.ui.le_ya_pass.text()
        if self.ya_login():
            cipher_key = Fernet.generate_key()
            cipher = Fernet(cipher_key)
            ya_bytes = json.dumps({'ya_email': self.ya_email, 'ya_pass': self.ya_pass}).encode()
            encrypted_bytes = cipher.encrypt(ya_bytes)
            with open("settings.bin", "wb") as file:
                pickle.dump(cipher_key, file, -1)
            settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                                 QSettings.IniFormat)
            arr = [("ya", encrypted_bytes)]
            for i, j in arr:
                settings.setValue(i, j)

    # Восстановление реквизитов входа
    def ya_restore(self):
        settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), \
                                          "settings.ini"), QSettings.IniFormat)
        try:
            cipher_key = pickle.load(open("settings.bin", "rb", -1))
        except (EOFError, FileNotFoundError):
            self.ui.le_ya_auth_state.setStyleSheet("color: rgb(255,0,0);")
            self.ui.le_ya_auth_state.setText("Необходимо авторизоваться")
        else:
            cipher = Fernet(cipher_key)
            encrypted_bytes = settings.value("ya")
            ya = json.loads(cipher.decrypt(encrypted_bytes).decode())
            self.ya_email = ya['ya_email']
            self.ya_pass = ya['ya_pass']
            self.ya_login()

# -----------------------------------------------------------------------
        
    def about(self):
        QMessageBox.about(
            self,
            "О FlickrToLJ", "FlickrToLJ версия 2.6.3\nСайт приложения: https://bitbucket.org/eigensinn/flickr_to_lj/"
                            "\nДата сборки: 22.04.2018\nНа основе библиотеки PyQt5\n")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('sgi')
    app.setStyleSheet(open(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "style.qss"), "r").read())
    mw = MainWindow()
    mw.show()
    sys.exit(app.exec_())
