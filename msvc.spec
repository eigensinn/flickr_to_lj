# -*- mode: python -*-

block_cipher = None


a = Analysis(['flickr_to_lj.py'],
             pathex=['D:\\Programming\\workspace\\flickr_to_lj',
			 'C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\Common7\\IDE\\Remote Debugger\\x64'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['numpy'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.zipfiles,
		  a.binaries,
          a.datas,
          name='flickr_to_lj',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )